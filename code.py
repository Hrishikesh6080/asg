import pandas as pd
df = pd.read_csv('employees.csv')
def display_data():
    print("\nEmployee data:")
    print(df)
while True:
    print("\nMenu:")
    print("1. Display employee data")
    print("2. Add new employee")
    print("3. Update employee designation")
    print("4. Delete employee")
    print("5. Exit")
    choice = input("Enter your choice (1/2/3/4/5): ")
    if choice == '1':
        display_data()
    elif choice == '2':
        new_employee = {
            'emp_name': input("Enter first name: "),
             'emp_no': int(input("Enter employee number: ")),
             'emp_dept': input("Enter employee department: "),
            'emp_desig': input("Enter designation: ")
        }
        df = df._append(new_employee, ignore_index=True)
        print("New employee added.")
        display_data()
    elif choice == '3':
        employee_no = int(input("Enter employee number to update designation: "))
        new_desig = input("Enter the new designation: ")
        df.loc[df['emp_no'] == employee_no, 'emp_desig'] = new_desig
        print("Designation updated.")
        display_data()
    elif choice == '4':
        employee_no = int(input("Enter employee number to delete: "))
        df = df[df['emp_no'] != employee_no]
        print("Employee deleted.")
        display_data()
    elif choice == '5':
        df.to_csv('employee_data.csv', index=False)
        print("Data saved, now exiting program.")
        break
    else:
        print("Invalid option entered. Please enter a valid option.")  
